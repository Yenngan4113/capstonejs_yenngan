import { showCart, showItemList } from "./controller/controller.js";
import { getService } from "./service/service.js";
let cart = [];
let productList = [];

const saveToStorage = () => {
  let cartJson = JSON.stringify(cart);
  localStorage.setItem("cart", cartJson);
};
let dataJson = localStorage.getItem("cart");

if (dataJson) {
  cart = JSON.parse(dataJson);
}

const renderProductList = () => {
  getService
    .getData()
    .then((res) => {
      let productList = res.data;

      showItemList(productList);
    })
    .catch((err) => {
      console.log(err);
    });
};

const renderCart = () => {
  let totalQuantity = cart.reduce((acc, curr) => {
    return acc + curr.quantity;
  }, 0);
  document.getElementById(
    "gioHang"
  ).innerHTML = `<i class="fa fa-shopping-cart"></i> Giỏ hàng (${totalQuantity})`;
};
renderProductList();
renderCart();
document.querySelector(".custom-select").addEventListener("change", (event) => {
  let searchValue = event.target.value == 1 ? true : false;
  getService
    .getData()
    .then((res) => {
      const searchList = res.data.filter((item) => {
        if (item.type == searchValue) {
          return item;
        }
      });
      if (searchList.length != 0 && event.target.value != 0) {
        showItemList(searchList);
      } else {
        showItemList(res.data);
      }
    })
    .catch((err) => {
      console.log(err);
    });
});

const handleAddToCart = (data) => {
  getService
    .getItemInfor(data)
    .then((res) => {
      let index = cart.findIndex((item) => {
        return item.id == res.data.id;
      });
      if (index == -1) {
        let newData = { ...res.data, quantity: 1 };

        cart.push(newData);
      } else {
        cart[index].quantity++;
      }
      renderCart();
      saveToStorage();
    })
    .catch((err) => {});
};
const handleShowCart = () => {
  showCart(cart);
  console.log(cart);
};
const handleDeleteItem = (data) => {
  let index = cart.findIndex((item) => {
    return item.id == data;
  });
  if (index != -1) {
    cart.splice(index, 1);
  }
  renderCart();

  saveToStorage();
  showCart(cart);
};
const handleQuantity = (id, value) => {
  let index = cart.findIndex((item) => {
    return item.id == id;
  });
  if (index != -1 && cart[index].quantity == 1 && value == -1) {
    cart.splice(index, 1);
  } else if (cart[index].quantity >= 1) {
    cart[index].quantity += value;
  }
  renderCart();
  showCart(cart);
  saveToStorage();
};

const handlePurchase = () => {
  cart = [];
  renderCart();
  saveToStorage();
};
window.handlePurchase = handlePurchase;
window.handleQuantity = handleQuantity;
window.handleAddToCart = handleAddToCart;
window.handleShowCart = handleShowCart;
window.handleDeleteItem = handleDeleteItem;
