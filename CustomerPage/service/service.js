const BASE_URL = "https://629edf248b939d3dc28925c2.mockapi.io/";

const getService = {
  getData: () => {
    return axios({
      url: `${BASE_URL}/product`,
      method: "GET",
    });
  },
  getItemInfor: (id) => {
    return axios({
      url: `${BASE_URL}/product/${id}`,
      method: "GET",
    });
  },
};

export { getService, BASE_URL };
