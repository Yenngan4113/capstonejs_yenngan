const showItemList = (list) => {
  let contentHTML = "";

  list.forEach((item) => {
    let contentDiv = /*html*/ `
    <div class="product_item">
        <div class="card d-inline-block border-none p-3 pt-4 cardItem" style=" border-radius:30px;  ">
        <div class="d-flex justify-content-center h-50">
  <img class="card-img-top margin-auto" src=${
    item.img
  } alt="Card image cap" style="width:100%; object-fit:contain"/></div>
  <div class="card-body">
    <h5 class="card-title" style="height:50px">${item.name}</h5>
    <p class="card-text text-danger" style="margin-bottom:0px; height:36px"><span class="font-weight-bold">Giá:</span> ${new Intl.NumberFormat(
      "de-DE"
    ).format(item.price)}$</p>
<div style="height: 40px; width:230px; overflow:hidden; text-overflow: ellipsis; white-space: nowrap" class="mb-2">${
      item.desc
    }

</div>
    <button class="btn btn-warning" onClick="handleAddToCart('${
      item.id
    }')">Add to cart</button>
  </div>
</div>
</div>`;
    contentHTML += contentDiv;
  });
  document.querySelector(".main").innerHTML = contentHTML;
};

export { showItemList };

const showCart = (list) => {
  list.sort((a, b) => {
    return a.id - b.id;
  });
  let totalPrice = list.reduce((acc, curr) => {
    return acc + curr.price * curr.quantity;
  }, 0);
  let contentHTML = "";
  list.forEach((item) => {
    let contentTrTag = /*html*/ `<tr >
    <td class="align-middle" >${item.id}</td>
    <td class="align-middle td_img"><img src=${
      item.img
    } style="width:48px"></td>
    <td class="align-middle">${item.name}</td>
    <td class="align-middle">${new Intl.NumberFormat("de-DE").format(
      item.price
    )}$</td>
    <td class="align-middle" style="width:150px">
    
    <p class="customized_btn bg-danger text-white" onClick="handleQuantity(${
      item.id
    },-1)">${item.quantity == 1 ? `<i class="fa fa-trash"></i>` : "-"}</p>
    ${item.quantity}
    <p class="customized_btn bg-success text-white" onClick="handleQuantity(${
      item.id
    },1)">+</p>
    
    </td>
    <td class="align-middle">
    
    
    ${new Intl.NumberFormat("de-DE").format(item.quantity * item.price)}$</td>
    <td class="align-middle"><button class="btn btn-danger" onClick="handleDeleteItem(${
      item.id
    })"><i class="fa fa-trash"></i></button></td>
    
    </tr>
    `;
    contentHTML += contentTrTag;
  });
  let contentFooter = `<tr>
    <td colspan="5" style="font-size:24px" class="text-danger text-right font-weight-bold">Thành tiền</td>
    <td style="font-size:24px" class="text-danger font-weight-bold">${new Intl.NumberFormat(
      "de-DE"
    ).format(totalPrice)}$</td>
  </tr>`;
  contentHTML += contentFooter;
  if (list.length == 0) {
    document.querySelector(
      "#cartTbody"
    ).innerHTML = `<td colspan="6" class="text-danger">Không có sản phẩm nào trong giỏ hàng</td>`;
  } else {
    document.querySelector("#cartTbody").innerHTML = contentHTML;
  }
};
export { showCart };
