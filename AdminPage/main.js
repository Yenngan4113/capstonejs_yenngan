import {
  getInforFromForm,
  showInforToForm,
  showProductList,
} from "./controller/controller.js";
import { ProductList } from "./model/model.js";
import { Validation } from "./model/Validationmodel.js";
import { getProductDataServices } from "./service/service.js";

let showLoading = () => {
  document.querySelector(".loading").style.display = "flex";
};
let hideLoading = () => {
  document.querySelector(".loading").style.display = "none";
};

let renderProductList = async () => {
  let result = await getProductDataServices.getData();
  let productList = result.data.map((Item) => {
    let productInfor = new ProductList(
      Item.name,
      Item.price,
      Item.screen,
      Item.backCamera,
      Item.frontCamera,
      Item.img,
      Item.desc,
      Item.type ? "iphone" : "samsung"
    );
    return { ...productInfor, id: Item.id };
  });

  showProductList(productList);
};

renderProductList();
const clearSpan = () => {
  document.querySelector("#nameNotif").innerText = "";
  document.querySelector("#priceNotif").innerText = "";
  document.querySelector("#screenNotif").innerText = "";
  document.querySelector("#backCameraNotif").innerText = "";
  document.querySelector("#frontCameraNotif").innerText = "";
  document.querySelector("#imgNotif").innerText = "";
  document.querySelector("#descNotif").innerText = "";
  document.querySelector("#typeNotif").innerText = "";
};
const addNewItem = () => {
  document.querySelector("#update_item").style.display = "none";
  document.querySelector("#add_newItem").style.display = "block";
  document.querySelector("#idsp").style.display = "none";
  document.querySelector("#product_name").value = "";
  document.querySelector("#product_price").value = "";
  document.querySelector("#product_screen").value = "";
  document.querySelector("#product_backCamera").value = "";
  document.querySelector("#product_frontCamera").value = "";
  document.querySelector("#product_img").value = "";
  document.querySelector("#product_desc").value = "";
  document.querySelector("#product_type").value = 0;
  clearSpan();
};
// Validation
let newValidation = new Validation();
let isValid = true;
let checkValidation = () => {
  let isValidName = newValidation.kiemTraRong("#product_name", "#nameNotif");
  let isValidPrice =
    newValidation.kiemTraRong("#product_price", "#priceNotif") &&
    newValidation.checkGia();
  let isValidImg = newValidation.kiemTraRong("#product_img", "#imgNotif");
  let isValidDesc = newValidation.kiemTraRong("#product_desc", "#descNotif");
  let isValidScreen = newValidation.kiemTraRong(
    "#product_screen",
    "#screenNotif"
  );
  let isValidFrontCamera = newValidation.kiemTraRong(
    "#product_frontCamera",
    "#frontCameraNotif"
  );
  let isValidBackCamera = newValidation.kiemTraRong(
    "#product_backCamera",
    "#backCameraNotif"
  );
  let isValidType = newValidation.checkType();
  isValid =
    isValidName &&
    isValidPrice &&
    isValidImg &&
    isValidDesc &&
    isValidScreen &&
    isValidFrontCamera &&
    isValidBackCamera &&
    isValidType;
};

const handleAdd = () => {
  showLoading();
  checkValidation();
  if (isValid) {
    document
      .querySelector("#add_newItem")
      .setAttribute("data-dismiss", "modal");

    let newProduct = getInforFromForm();
    getProductDataServices
      .postNewProduct(newProduct)
      .then((res) => {
        hideLoading();
        console.log(isValid);
        renderProductList();
      })
      .catch((err) => {
        hideLoading();
        console.log(err);
      });
  } else {
    hideLoading();
  }
};

const handleDelete = (id) => {
  showLoading();
  getProductDataServices
    .deleteProduct(id)
    .then((res) => {
      hideLoading();
      renderProductList();
    })
    .catch((err) => {
      hideLoading();

      console.log(err);
    });
};

const handleEdit = (id) => {
  clearSpan();

  document.querySelector("#add_newItem").style.display = "none";
  document.querySelector("#update_item").style.display = "block";
  document.querySelector("#idsp").style.display = "block";
  document.querySelector("#idsp span").innerHTML = ` ${id}`;
  showLoading();
  getProductDataServices
    .getProductInfor(id)
    .then((res) => {
      hideLoading();
      showInforToForm(res.data);
    })
    .catch((err) => {
      hideLoading();

      console.log(err);
    });
};
const handleUpdate = () => {
  let id = +document.querySelector("#idsp span").innerHTML;
  let updatedProduct = getInforFromForm();
  showLoading();
  checkValidation();
  if (isValid) {
    document
      .querySelector("#update_item")
      .setAttribute("data-dismiss", "modal");
    getProductDataServices
      .pushProductInfor(id, updatedProduct)
      .then((res) => {
        hideLoading();

        renderProductList();
      })
      .catch((err) => {
        hideLoading();

        console.log(err);
      });
  } else {
    hideLoading();
  }
};
document.querySelector("#search_input").addEventListener("focus", () => {
  renderProductList();
  document.querySelector("#search_notif").innerText = "";
});
const handleSearch = () => {
  let inputValue = document.querySelector("#search_input").value;

  showLoading();
  getProductDataServices
    .getData()
    .then((res) => {
      hideLoading();
      let searchList = res.data.filter((Item) => {
        return Item.name == inputValue;
      });

      if (searchList.length != 0) {
        showProductList(searchList);
      } else {
        document.querySelector("#search_notif").innerText =
          "Không tìm thấy thông tin";
        renderProductList();
      }
    })
    .catch((err) => {
      hideLoading();
      console.log(err);
    });
};
window.handleAdd = handleAdd;
window.handleDelete = handleDelete;
window.handleEdit = handleEdit;
window.addNewItem = addNewItem;
window.handleUpdate = handleUpdate;
window.handleSearch = handleSearch;
