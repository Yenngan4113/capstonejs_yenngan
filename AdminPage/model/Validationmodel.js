function Validation() {
  this.kiemTraRong = function (idCheck, idError) {
    let inputValue = document.querySelector(idCheck).value.trim();
    if (inputValue) {
      document.querySelector(idError).innerText = "";
      return true;
    } else {
      document.querySelector(idError).style.display = "block";
      document.querySelector(idError).innerText = "Không được để rỗng";
      return false;
    }
  };
  this.checkGia = function () {
    let inputValue = document.querySelector("#product_price").value;
    let number = /^[0-9]+$/;
    if (inputValue.match(number)) {
      document.querySelector("#priceNotif").innerText = "";
      return true;
    } else {
      document.querySelector("#priceNotif").innerText = "Vui lòng nhập số";
      return false;
    }
  };

  this.checkType = function () {
    let input = document.querySelector("#product_type").value;

    if (input == "1" || input == "2") {
      document.querySelector("#typeNotif").innerText = "";

      return true;
    } else {
      document.querySelector("#typeNotif").innerText =
        "Vui lòng chọn loại điện thoại";
      return false;
    }
  };
}

export { Validation };
