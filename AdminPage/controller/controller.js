import { ProductList } from "../model/model.js";

const getInforFromForm = () => {
  let name = document.querySelector("#product_name").value;
  let price = +document.querySelector("#product_price").value;
  let screen = document.querySelector("#product_screen").value;
  let backCamera = document.querySelector("#product_backCamera").value;
  let frontCamera = document.querySelector("#product_frontCamera").value;
  let img = document.querySelector("#product_img").value;
  let desc = document.querySelector("#product_desc").value;
  let type =
    +document.querySelector("#product_type").value === 1 ? true : false;

  return {
    name: name,
    price: price,
    screen: screen,
    backCamera: backCamera,
    frontCamera: frontCamera,
    img: img,
    desc: desc,
    type: type,
  };
};

export { getInforFromForm };

const showInforToForm = (data) => {
  document.querySelector("#product_name").value = data.name;
  document.querySelector("#product_price").value = data.price;
  document.querySelector("#product_screen").value = data.screen;
  document.querySelector("#product_backCamera").value = data.backCamera;
  document.querySelector("#product_frontCamera").value = data.frontCamera;
  document.querySelector("#product_img").value = data.img;
  document.querySelector("#product_desc").value = data.desc;
  document.querySelector("#product_type").value =
    data.type === true ? "1" : "2";
};

export { showInforToForm };

const showProductList = (list) => {
  let contentHTML = "";
  list.forEach((Item) => {
    let formatPrice = new Intl.NumberFormat("vi-VI").format(Item.price);
    let contentTrTag = /*html*/ `<tr >
    <td class="align-middle">${Item.id}</td>
    <td class="align-middle">${Item.name}</td>
    <td class="align-middle text-center">$${formatPrice}</td>
    <td class="align-middle text-center"><img src='${Item.img}' style="width:80px"/></td>
    <td class="align-middle" style="width: 300px">${Item.desc}</td>
    <td class="align-middle text-center" style="width:150px">
    <button class="btn btn-danger mr-1 inline-block" onclick="handleDelete('${Item.id}')">Xóa</button>
    <button class="btn btn-warning inline-block" onclick="handleEdit('${Item.id}')" data-toggle="modal"
    data-target="#modalAdmin">Sửa</button>
    </td> 
    </tr>`;
    contentHTML += contentTrTag;
  });

  document.querySelector("#tbody").innerHTML = contentHTML;
};

export { showProductList };
