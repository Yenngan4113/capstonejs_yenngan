const BASE_URL = "https://629edf248b939d3dc28925c2.mockapi.io/product";
const getProductDataServices = {
  getData: () => {
    return axios.get(`${BASE_URL}`);
  },
  postNewProduct: (data) => {
    return axios.post(`${BASE_URL}`, data);
  },
  deleteProduct: (id) => {
    return axios.delete(`${BASE_URL}/${id}`);
  },
  getProductInfor: (id) => {
    return axios.get(`${BASE_URL}/${id}`);
  },
  pushProductInfor: (id, data) => {
    return axios.put(`${BASE_URL}/${id}`, data);
  },
};
export { getProductDataServices };
